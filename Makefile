local-run-skip-test:
	 gradle clean bootRun -x test  --scan

local-run-scan:
	gradle bootRun --scan

start:
	docker-compose up

stop:
	docker-compose down

test:
	gradle clean test

report:
	gradle clean package

