package com.bol.candidate.cali.config;

import lombok.RequiredArgsConstructor;
import org.springdoc.core.GroupedOpenApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;

@RequiredArgsConstructor
@Configuration
public class SwaggerConfiguration {

  public static final String GAME_TAG = "KHALA service";

  @Bean
  public GroupedOpenApi publicApi() {
    return GroupedOpenApi.builder().group("khala-public").pathsToMatch("/v1/**").build();
  }

  protected void addResourceHandlers(ResourceHandlerRegistry registry) {
    registry
        .addResourceHandler("swagger-ui.html")
        .addResourceLocations("classpath:/META-INF/resources/");

    registry
        .addResourceHandler("/webjars/**")
        .addResourceLocations("classpath:/META-INF/resources/webjars/");
  }
}
