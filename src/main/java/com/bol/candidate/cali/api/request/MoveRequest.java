package com.bol.candidate.cali.api.request;

import com.bol.candidate.cali.data.enums.Turn;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.With;
import org.springframework.data.mongodb.core.mapping.MongoId;

@Builder
@Getter
@Setter
@Schema(name = "Move")
public class MoveRequest {
  @With
  @Schema(nullable = false, hidden = true)
  @MongoId
  private String id;

  @Schema(description = "related Board State Id", nullable = false)
  private String stateId;

  @Schema(description = "FIRST~SECOND_PLAYER")
  private Turn player;

  @Schema(maximum = "5", nullable = false, minimum = "0")
  private Integer moveCellIndex;
}
