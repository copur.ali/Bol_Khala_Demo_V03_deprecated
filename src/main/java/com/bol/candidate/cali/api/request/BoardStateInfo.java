package com.bol.candidate.cali.api.request;

import com.bol.candidate.cali.data.entity.Row;
import com.bol.candidate.cali.data.enums.Turn;

public interface BoardStateInfo {
  Row getFirstPlayerRow();

  Row getSecondPlayerRow();

  Turn getTurn();

  boolean isIsWinnerFound();
}
