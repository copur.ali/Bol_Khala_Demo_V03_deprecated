package com.bol.candidate.cali.api.request;

import com.bol.candidate.cali.data.entity.Game;
import com.bol.candidate.cali.data.enums.GameActivity;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "khala_game")
public class GameRequest extends Game {

  public GameRequest(final String gameId) {
    super(gameId);
  }

  public void joinPlayers(final String playerOne, final String playerTwo) {
    setPlayerOneId((playerOne));
    setPlayerTwoId((playerTwo));
    setGameActivity(GameActivity.ACTIVE.ordinal());
    // IntStream.range(0, 1).mapToObj(i ->
    // joinPlayers(playerOne,playerTwo)).collect(Collectors.toList());
  }

  public void completeGame(final String winningPlayer) {
    setGameActivity(GameActivity.COMPLETED.ordinal());
    setWiningPlayerId(winningPlayer);
  }

  public void suspendGame() {
    setGameActivity(GameActivity.SUSPENDED.ordinal());
  }
}
