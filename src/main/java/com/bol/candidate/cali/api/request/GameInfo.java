package com.bol.candidate.cali.api.request;

import java.time.LocalDateTime;

public interface GameInfo {
  String getId();

  String getGameId();

  LocalDateTime getStartTime();

  LocalDateTime getEndTime();

  String getPlayerOneId();

  String getPlayerTwoId();

  String getWiningPlayerId();

  Integer getGameActivity();

  String getBoardStateId();
}
