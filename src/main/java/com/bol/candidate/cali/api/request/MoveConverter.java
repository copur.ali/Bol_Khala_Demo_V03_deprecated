package com.bol.candidate.cali.api.request;

import com.bol.candidate.cali.api.service.CellService;
import com.bol.candidate.cali.data.enums.Turn;
import lombok.Builder;
import lombok.Getter;
import lombok.With;

@With
@Getter
@Builder
public class MoveConverter {
  private NextMove nextMove;
  private Integer pieces;

  public MoveConverter next() {
    return withNextMove(getUpdatedNext()).withPieces(getUpdatedPieces());
  }

  public NextMove getUpdatedNext() {
    if (nextMove.getIndex().equals(CellService.TREASURE_CELL_INDEX)) {
      return NextMove.builder()
          .player(
              nextMove.getPlayer().equals(Turn.FIRST_PLAYER)
                  ? Turn.SECOND_PLAYER
                  : Turn.FIRST_PLAYER)
          .index(0)
          .build();
    } else {
      return nextMove.withIndex(nextMove.getIndex() + 1);
    }
  }

  public Integer getUpdatedPieces() {
    return this.pieces - 1;
  }
}
