package com.bol.candidate.cali.api.request;

import com.bol.candidate.cali.data.enums.Turn;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Getter;
import lombok.With;

@With
@Getter
@Builder
@Schema
public class NextMove {
  @Schema private Integer index;
  @Schema private Turn player;
}
