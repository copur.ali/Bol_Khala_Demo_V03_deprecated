package com.bol.candidate.cali.api.service;

import com.bol.candidate.cali.api.request.MoveConverter;
import com.bol.candidate.cali.api.request.MoveRequest;
import com.bol.candidate.cali.api.request.NextMove;
import com.bol.candidate.cali.data.entity.BoardState;
import com.bol.candidate.cali.data.entity.Cell;
import com.bol.candidate.cali.data.enums.Turn;
import com.bol.candidate.cali.data.repo.BoardStateRepository;
import com.bol.candidate.cali.exception.*;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class BoardStateService {

  @NonNull private BoardStateRepository stateRepository;

  public BoardState create() {

    return this.stateRepository.save(new BoardState());
  }

  public BoardState findById(String id) {

    return this.load(id);
  }

  public BoardState move(MoveRequest request) {

    final BoardState state = this.load(request.getStateId());

    this.isValidMove(request, state);

    final MoveConverter move =
        MoveConverter.builder()
            .nextMove(
                NextMove.builder()
                    .index(request.getMoveCellIndex() + 1)
                    .player(request.getPlayer())
                    .build())
            .pieces(CellService.grabAllPiecesInCell(request, state))
            .build();

    return this.stateRepository.save(this.convertMoveToCells(state, move));
  }

  private BoardState load(String id) {
    return this.stateRepository
        .findById(id)
        .orElseThrow(() -> new BoardStateNotFoundException("Can not find the game by id" + id));
  }

  private BoardState create(BoardState state) {

    return this.stateRepository.save(state);
  }

  private BoardState convertMoveToCells(final BoardState state, final MoveConverter moveConverter) {
    if (moveConverter.getPieces() > 0) {
      this.updateCellPieces(state, moveConverter);
      return this.convertMoveToCells(state, moveConverter.next());
    }

    if (this.isLastPieceInTreasureCell(moveConverter)) {
      state.turnNext();
    }

    if (state.isZeroRow()) {
      state.completeGame();
    }

    return state;
  }

  private boolean isLastPieceInTreasureCell(final MoveConverter moveConverter) {
    return !CellService.STARTER_CELL_INDEX.equals(moveConverter.getNextMove().getIndex());
  }

  private void updateCellPieces(final BoardState state, final MoveConverter moveConverter) {
    final Cell cell = CellService.getCell(moveConverter.getNextMove(), state);

    if (moveConverter.getPieces() == 1 && cell.getPieces().equals(0) && !cell.isTreasureCell()) {
      cell.setPieces(CellService.capturePieces(state, moveConverter));
    } else {
      cell.setPieces(cell.getPieces() + 1);
    }
  }

  private void validateInTreasureCell(final MoveRequest request) {
    if (request.getMoveCellIndex().equals(CellService.TREASURE_CELL_INDEX)) {
      throw new IllegalMoveException();
    }
  }

  private void validateCellPieces(final BoardState state, final MoveRequest request) {
    if (CellService.getCell(request, state).getPieces() < 1) {
      throw new EmptyCellException();
    }
  }

  private void validatePlayer(final BoardState state, final Turn player) {
    if (!state.getTurn().equals(player)) {
      throw new UnauthorizedPlayerException();
    }
  }

  private void validateActiveGame(final BoardState state) {
    if (state.isWinnerFound()) {
      throw new GameIsNotActiveException();
    }
  }

  private void isValidMove(final MoveRequest request, final BoardState state) {
    this.validateActiveGame(state);
    this.validatePlayer(state, request.getPlayer());
    this.validateInTreasureCell(request);
    this.validateCellPieces(state, request);
  }
}
