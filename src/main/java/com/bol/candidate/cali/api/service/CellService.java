package com.bol.candidate.cali.api.service;

import com.bol.candidate.cali.api.request.MoveConverter;
import com.bol.candidate.cali.api.request.MoveRequest;
import com.bol.candidate.cali.api.request.NextMove;
import com.bol.candidate.cali.data.entity.BoardState;
import com.bol.candidate.cali.data.entity.Cell;
import com.bol.candidate.cali.data.entity.Row;
import com.bol.candidate.cali.data.enums.Turn;
import org.springframework.stereotype.Service;

@Service
public class CellService {

  public static final Integer TREASURE_CELL_INDEX = 6; // nonReturn Tresuary Treasure Cell
  public static final Integer STARTER_CELL_INDEX = 0;


  public static Cell getCell(final NextMove nextMove, final BoardState state) {
    if (Turn.FIRST_PLAYER.equals(nextMove.getPlayer())) {
      return TREASURE_CELL_INDEX.equals(nextMove.getIndex())
          ? state.getFirstPlayerRow().getTreasureCell()
          : getCell(state.getFirstPlayerRow(), nextMove.getIndex());
    } else {
      return CellService.TREASURE_CELL_INDEX.equals(nextMove.getIndex())
          ? state.getSecondPlayerRow().getTreasureCell()
          : getCell(state.getSecondPlayerRow(), nextMove.getIndex());
    }
  }

  public static Cell getCell(final MoveRequest request, final BoardState state) {
    return Turn.FIRST_PLAYER.equals(request.getPlayer())
        ? getCell(state.getFirstPlayerRow(), request.getMoveCellIndex())
        : getCell(state.getSecondPlayerRow(), request.getMoveCellIndex());
  }

  private static Cell getCell(final Row playerRow, final Integer cellIndex) {
    return playerRow.getCells().get(cellIndex);
  }

  public static Integer capturePieces(final BoardState state, final MoveConverter moveConverter) {
    final Cell opponentRowCell = getOpponentRowCell(moveConverter, state);
    final Integer pieces = opponentRowCell.getPieces() + 1;
    opponentRowCell.setPieces(0);

    return pieces;
  }

  private static Cell getOpponentRowCell(
      final MoveConverter moveConverter, final BoardState state) {
    if (Turn.FIRST_PLAYER.equals(moveConverter.getNextMove().getPlayer())) {
      return getCell(
          NextMove.builder()
              .index(getOpponentCellIndex(moveConverter.getNextMove().getIndex()))
              .player(Turn.SECOND_PLAYER)
              .build(),
          state);
    } else {
      return getCell(
          NextMove.builder()
              .index(getOpponentCellIndex(moveConverter.getNextMove().getIndex()))
              .player(Turn.FIRST_PLAYER)
              .build(),
          state);
    }
  }

  private static Integer getOpponentCellIndex(Integer index) {
    return 5 - index;
  }

  public static Integer grabAllPiecesInCell(MoveRequest request, BoardState state) {
    Cell cell = getCell(request, state);

    Integer pieces = cell.getPieces();
    cell.setPieces(0);
    return pieces;
  }
}
