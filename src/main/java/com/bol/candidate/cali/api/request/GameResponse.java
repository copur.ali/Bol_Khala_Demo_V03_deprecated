package com.bol.candidate.cali.api.request;

import com.bol.candidate.cali.data.entity.Game;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import lombok.Getter;
import lombok.Setter;

@JsonInclude(JsonInclude.Include.NON_NULL)
@Getter
@Setter
@ApiResponse
public class GameResponse extends Game {

  public GameResponse(final String gameId) {
    super(gameId);
  }
}
