package com.bol.candidate.cali.api.service;

import com.bol.candidate.cali.api.request.GameRequest;
import com.bol.candidate.cali.api.request.GameResponse;
import com.bol.candidate.cali.data.entity.Game;
import com.bol.candidate.cali.data.enums.GameActivity;
import com.bol.candidate.cali.data.enums.Turn;
import com.bol.candidate.cali.data.repo.GameRepository;
import com.bol.candidate.cali.exception.GameNotFoundException;
import io.swagger.v3.oas.annotations.Hidden;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.projection.SpelAwareProxyProjectionFactory;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.UUID;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class GameService {

  @NonNull private final GameRepository gameRepository;

  private final BoardStateService boardStateService;

  @NonNull private final SpelAwareProxyProjectionFactory projectionFactory;

  public void createMultiPlayerGame(GameRequest gameRequest) {
    final Game game = initSoloGame(gameRequest);
    game.setPlayerTwoId(gameRequest.getPlayerTwoId());
  }

  public void createSoloGame(GameRequest gameRequest) {
    Game game = this.initSoloGame(gameRequest);
  }

  @Hidden
  public GameResponse initSoloGame(GameRequest gameRequest) {

    Game game = new Game(UUID.randomUUID().toString());
    game.setGameActivity(GameActivity.SOLO_CREATED.ordinal());
    game.setPlayerOneId(gameRequest.getPlayerOneId());
    game.setStartTime(LocalDateTime.now());
    game.setGameId(gameRequest.getGameId());
    gameRepository.save(game);
    final GameResponse gameResponse = new GameResponse(gameRequest.getGameId());
    gameResponse.setGameId(game.getGameId());
    gameResponse.setGameActivity(game.getGameActivity());
    gameResponse.setStartTime(game.getStartTime());
    gameResponse.setPlayerOneId(game.getPlayerOneId());

    return gameResponse;
  }

  public GameResponse findGameByGameId(String gameId) {

    return (GameResponse)
        gameRepository
            .getGameByGameId(gameId)
            .orElseThrow(() -> new GameNotFoundException("Can not find the game by id " + gameId));
  }

  public GameResponse loadActiveGameWithBoardStateId(String requestedBoardStateId) {

    return (GameResponse)
        gameRepository
            .getGameByBoardStateIdAndGameActivity(
                requestedBoardStateId, GameActivity.ACTIVE.ordinal())
            .orElseThrow(
                () ->
                    new GameNotFoundException(
                        "Can not find the game by board id " + requestedBoardStateId));
  }

  private GameResponse joinGameSecondPlayer(final String gameId, final String playerTwoId) {
    Game game = this.loadGame(gameId);
    game.setGameId(gameId);
    game.setPlayerTwoId(playerTwoId);
    game.setGameActivity(GameActivity.ACTIVE.ordinal());
    gameRepository.save(game);

    final GameResponse gameResponse = new GameResponse(gameId);
    gameResponse.setGameId(game.getGameId());
    gameResponse.setGameActivity(game.getGameActivity());
    gameResponse.setStartTime(game.getStartTime());
    gameResponse.setPlayerOneId(game.getPlayerOneId());
    gameResponse.setPlayerTwoId(game.getPlayerTwoId());

    return gameResponse;
  }

  private void updateGameActivity(final String gameId, final GameActivity activity) {
    final Game game =
        gameRepository
            .getGameByGameId(gameId)
            .orElseThrow(() -> new GameNotFoundException("Can not find the game by id" + gameId));
    game.setGameActivity(activity.ordinal());
  }

  private GameResponse loadGame(String gameId) {

    return (GameResponse)
        gameRepository
            .findById(gameId)
            .orElseThrow(() -> new GameNotFoundException("Can not find the game by id" + gameId));
  }

  private void finishGame(String gameId) {

    Game game =
        gameRepository
            .findById(gameId)
            .orElseThrow(() -> new GameNotFoundException("Can not find the game by id" + gameId));
    game.setEndTime(LocalDateTime.now());

    boolean isWinFirst =
        boardStateService.findById(game.getBoardStateId()).getTurn().equals(Turn.FIRST_PLAYER);
    if (isWinFirst) {
      game.setWiningPlayerId(game.getPlayerOneId());
    } else {
      game.setWiningPlayerId(game.getPlayerTwoId());
    }

    game.suspendGame();
    game.setGameActivity(GameActivity.COMPLETED.ordinal());
    gameRepository.save(game);
  }

}
