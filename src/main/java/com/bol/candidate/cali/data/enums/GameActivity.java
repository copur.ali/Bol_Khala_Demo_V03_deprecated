package com.bol.candidate.cali.data.enums;

public enum GameActivity {
  ACTIVE,
  CREATED,
  SOLO_CREATED,
  SUSPENDED,
  COMPLETED

}
