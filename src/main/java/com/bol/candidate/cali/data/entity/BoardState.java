package com.bol.candidate.cali.data.entity;

import com.bol.candidate.cali.data.enums.Turn;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import org.springframework.boot.test.autoconfigure.data.mongo.AutoConfigureDataMongo;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@AutoConfigureDataMongo
@Document(collection = "khala_boardstate")
public class BoardState {
  @Schema @Id private String id;

  private Row firstPlayerRow;

  private Row secondPlayerRow;
  @Schema private Turn turn;
  @Schema private boolean isWinnerFound;

  public BoardState() {
    firstPlayerRow = new Row();
    secondPlayerRow = new Row();
    turn = Turn.FIRST_PLAYER;
  }

  public void turnNext() {
    if (turn.equals(Turn.FIRST_PLAYER)) {
      turn = Turn.SECOND_PLAYER;
    } else {
      turn = Turn.FIRST_PLAYER;
    }
  }

  public void completeGame() {
    firstPlayerRow.zeroSum();
    secondPlayerRow.zeroSum();
    isWinnerFound = true;
  }

  public boolean isZeroRow() {
    return this.firstPlayerRow.isSetTreasureCell() || this.secondPlayerRow.isSetTreasureCell();
  }
}
