package com.bol.candidate.cali.data.repo;

import com.bol.candidate.cali.data.entity.BoardState;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BoardStateRepository extends MongoRepository<BoardState, String> {}
