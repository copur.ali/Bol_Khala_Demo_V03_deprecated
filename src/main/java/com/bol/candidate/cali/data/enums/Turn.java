package com.bol.candidate.cali.data.enums;

public enum Turn {
  FIRST_PLAYER,
  SECOND_PLAYER
}
