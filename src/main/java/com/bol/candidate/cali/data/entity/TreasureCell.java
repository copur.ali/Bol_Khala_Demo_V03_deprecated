package com.bol.candidate.cali.data.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.boot.test.autoconfigure.data.mongo.AutoConfigureDataMongo;

@Data
@AutoConfigureDataMongo
@EqualsAndHashCode(callSuper = true)
public class TreasureCell extends Cell {

  public TreasureCell() {
    pieces = (0);
  }

  @Override
  public boolean isTreasureCell() {
    return true;
  }
}
