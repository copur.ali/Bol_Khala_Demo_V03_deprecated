package com.bol.candidate.cali.data.entity;

import com.bol.candidate.cali.data.enums.GameActivity;
import lombok.Data;
import org.springframework.boot.test.autoconfigure.data.mongo.AutoConfigureDataMongo;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDateTime;

@Data
@AutoConfigureDataMongo
@Document(collection = "khala_game")
public class Game {
  @Id private String id;
  @Indexed private String gameId;
  private LocalDateTime startTime;
  private LocalDateTime endTime;

  private String playerOneId;
  private String playerTwoId;
  private String winingPlayerId;
  @Indexed private Integer gameActivity;
  @Indexed private String boardStateId;

  public Game(final String gameId) {
    this.gameId = (gameId);
  }

  public void joinPlayers(String playerOne, String playerTwo) {
    playerOneId = (playerOne);
    playerTwoId = (playerTwo);
    gameActivity = GameActivity.ACTIVE.ordinal();
    // IntStream.range(0, 1).mapToObj(i ->
    // joinPlayers(playerOne,playerTwo)).collect(Collectors.toList());
  }

  public void completeGame() {
    gameActivity = GameActivity.COMPLETED.ordinal();
    winingPlayerId = playerOneId;
  }

  public void suspendGame() {

    gameActivity = GameActivity.SUSPENDED.ordinal();
  }

}
