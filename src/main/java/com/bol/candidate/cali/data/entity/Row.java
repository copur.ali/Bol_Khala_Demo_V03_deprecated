package com.bol.candidate.cali.data.entity;

import lombok.Data;
import org.springframework.boot.test.autoconfigure.data.mongo.AutoConfigureDataMongo;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Data
@AutoConfigureDataMongo
public class Row {
  private List<Cell> cells;
  private TreasureCell treasureCell;

  public Row() {
    this.cells = IntStream.range(0, 6).mapToObj(i -> new Cell()).collect(Collectors.toList());
    this.treasureCell = new TreasureCell();
  }

  public boolean isSetTreasureCell() {
    return cells.stream().allMatch(cell -> cell.getPieces().equals(0));
  }

  public void zeroSum() {
    this.getTreasureCell()
        .setPieces(
            this.getTreasureCell().getPieces()
                + this.cells.stream().mapToInt(Cell::getPieces).sum());
    this.cells.forEach(cell -> cell.setPieces(0));
  }
}
