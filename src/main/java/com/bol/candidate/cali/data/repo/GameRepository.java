package com.bol.candidate.cali.data.repo;

import com.bol.candidate.cali.data.entity.Game;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface GameRepository extends MongoRepository<Game, String> {

  Optional<Game> getGameByBoardStateIdAndGameActivity(String stateId, int gameActivity);

  Optional<Game> getGameByGameId(String gameId);
}
