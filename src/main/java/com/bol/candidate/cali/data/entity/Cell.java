package com.bol.candidate.cali.data.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.With;
import org.springframework.boot.test.autoconfigure.data.mongo.AutoConfigureDataMongo;

@Data
@With
@AutoConfigureDataMongo
@AllArgsConstructor
public class Cell {
  protected Integer pieces;

  public Cell() {

    pieces = 6;
  }

  public boolean isTreasureCell() {

    return false;
  }
}
