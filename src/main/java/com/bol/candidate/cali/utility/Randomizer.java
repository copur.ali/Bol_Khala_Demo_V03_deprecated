package com.bol.candidate.cali.utility;

import java.math.BigInteger;
import java.security.SecureRandom;

public class Randomizer {
  private static final SecureRandom secureRandom = new SecureRandom();

  public static String random() {
    return new BigInteger(40, secureRandom).toString(32).toUpperCase();
  }

  public static Long randomLong() {
    return Long.valueOf(new BigInteger(40, secureRandom).toString(32).toUpperCase());
  }
}
