package com.bol.candidate.cali.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class GameNotFoundException extends ResponseStatusException {

  public GameNotFoundException(String gameId) {

    super(HttpStatus.NOT_FOUND, "Game is not available with Id:" + gameId);
  }
}
