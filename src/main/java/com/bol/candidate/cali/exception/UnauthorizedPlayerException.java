package com.bol.candidate.cali.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class UnauthorizedPlayerException extends ResponseStatusException {

  public UnauthorizedPlayerException() {

    super(HttpStatus.UNAUTHORIZED, "Move is not Acceptable, Wait your turn!");
  }
}
