package com.bol.candidate.cali.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class IllegalMoveException extends ResponseStatusException {
  public IllegalMoveException() {

    super(HttpStatus.NOT_ACCEPTABLE, "Move is not Acceptable");
  }
}
