package com.bol.candidate.cali.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class BoardStateNotFoundException extends ResponseStatusException {

  public BoardStateNotFoundException(String reasonId) {

    super(HttpStatus.NOT_FOUND, "Board State  is not available" + reasonId);
  }
}
