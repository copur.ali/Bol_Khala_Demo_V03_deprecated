package com.bol.candidate.cali.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class GameIsNotActiveException extends ResponseStatusException {
  public GameIsNotActiveException() {

    super(HttpStatus.NOT_FOUND, "Game is not Active");
  }
}
