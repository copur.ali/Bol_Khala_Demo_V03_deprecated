package com.bol.candidate.cali.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class EmptyCellException extends ResponseStatusException {
  public EmptyCellException() {

    super(HttpStatus.NOT_FOUND, "EmptyCell is found");
  }
}
