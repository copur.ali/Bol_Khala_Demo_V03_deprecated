package com.bol.candidate.cali.controller;

import com.bol.candidate.cali.api.request.GameRequest;
import com.bol.candidate.cali.api.request.GameResponse;
import com.bol.candidate.cali.api.request.GameResponseInfo;
import com.bol.candidate.cali.api.service.BoardStateService;
import com.bol.candidate.cali.api.service.GameService;
import com.bol.candidate.cali.data.entity.BoardState;
import com.bol.candidate.cali.exception.GameNotFoundException;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.projection.SpelAwareProxyProjectionFactory;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@OpenAPIDefinition(info = @Info(title = "Khala Game API", version = "v1"))
@Tag(name = "game", description = "Khala Game API v0.1")
@RequestMapping(path = "/v1/game", produces = MediaType.APPLICATION_JSON_VALUE)
public class GameController {

  @NonNull private final SpelAwareProxyProjectionFactory projectionFactory;

  private GameService gameService;

  private BoardStateService boardService;

  @Autowired
  public GameController(final SpelAwareProxyProjectionFactory projectionFactory) {
    this.projectionFactory = projectionFactory;
  }

  // GET /game/Game/
  @RequestMapping(method = RequestMethod.GET)
  public GameResponseInfo getGames(@PathVariable final String gameId) throws GameNotFoundException {
    return projectionFactory.createProjection(
        GameResponseInfo.class, gameService.findGameByGameId(gameId));
  }

  @RequestMapping(method = RequestMethod.POST)
  public GameResponseInfo initGame(
      @RequestBody GameRequest gameRequest) { // throws GameNotFoundException,
    final GameResponse newGame = gameService.initSoloGame(gameRequest);
    if (!gameRequest.getPlayerTwoId().isEmpty() || !gameRequest.getPlayerTwoId().isBlank()) {
      gameService.createMultiPlayerGame(gameRequest);
    }
    gameService.createSoloGame(gameRequest);
    final BoardState boardState = boardService.create();
    newGame.setBoardStateId(boardState.getId());

    return projectionFactory.createProjection(GameResponseInfo.class, newGame);
  }
}
