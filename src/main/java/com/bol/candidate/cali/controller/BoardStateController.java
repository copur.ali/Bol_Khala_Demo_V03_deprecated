package com.bol.candidate.cali.controller;

import com.bol.candidate.cali.api.request.MoveRequest;
import com.bol.candidate.cali.api.service.BoardStateService;
import com.bol.candidate.cali.data.entity.BoardState;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.data.projection.SpelAwareProxyProjectionFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@OpenAPIDefinition(info = @Info(title = "Khala BoardState API", version = "v1"))
@Tag(name = "boardstate", description = "Khala BoardState API")
@RequestMapping(path = "/v1/boardstate", produces = MediaType.APPLICATION_JSON_VALUE)
public class BoardStateController {

  @NonNull private BoardStateService stateService;

  @NonNull private SpelAwareProxyProjectionFactory projectionFactory;

  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  @Operation(summary = "new boardState", description = "BoardState created")
  @ApiResponses(@ApiResponse(responseCode = "201", description = "BoardState created"))
  public BoardState createBoard() {
    return projectionFactory.createProjection(BoardState.class, stateService.create());
  }

  @PutMapping(path = "/{id}")
  @Operation(
      summary = "A new Move is applied in boardState",
      description = "BoardState updated by move")
  @ApiResponses({
    @ApiResponse(
        responseCode = "200",
        description = "Move Applied",
        content =
            @Content(
                mediaType = "application/json",
                schema = @Schema(implementation = BoardState.class))),
    @ApiResponse(responseCode = "404", description = "BoardState id not found"),
    @ApiResponse(responseCode = "500", description = "Invalid move")
  })
  public BoardState move(
      @PathVariable final String id, @Valid @RequestBody final MoveRequest request) {
    return projectionFactory.createProjection(
        BoardState.class, stateService.move(request.withId(id)));
  }

  @GetMapping(path = "/{id}")
  @Operation(summary = "Find by boardState id", description = "given the found boardstate")
  @ApiResponses({
    @ApiResponse(
        responseCode = "200",
        description = "BoardState loaded",
        content =
            @Content(
                mediaType = "application/json",
                schema = @Schema(implementation = BoardState.class))),
    @ApiResponse(responseCode = "404", description = "BoardState id not found")
  })
  public BoardState findById(@PathVariable final String id) {
    return projectionFactory.createProjection(BoardState.class, stateService.findById(id));
  }

  /*@Controller
  class SampleController {

      private final ProjectionFactory projectionFactory;

      @Autowired
      public SampleController(ProjectionFactory projectionFactory) {
          this.projectionFactory = projectionFactory;
      }

      @PreAuthorize(value = "hasRole('ROLE_ADMIN')")
      @RequestMapping(value = "/users/employee")
      public Page<?> listEmployees(Pageable pageable) {

          return usersRepository.findEmployeeUsers(pageable).//
                  map(user -> projectionFactory.createProjection(Projection.class, user);
      }
  }*/
}
