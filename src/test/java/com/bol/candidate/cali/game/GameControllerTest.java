package com.bol.candidate.cali.game;

import com.bol.candidate.cali.api.request.GameRequest;
import com.bol.candidate.cali.api.request.GameResponse;
import com.bol.candidate.cali.api.request.MoveRequest;
import com.bol.candidate.cali.data.enums.Turn;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.web.client.RestTemplate;

import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.net.URI;
import java.net.URISyntaxException;

// @EnableAutoConfiguration(exclude = EmbeddedMongoAutoConfiguration.class)

@ActiveProfiles("test")

// @ExtendWith(SpringExtension.class)
@DisplayName("GameControllerTest Integration Test")
public class GameControllerTest {
  private static String gameID = "demo";
  @LocalServerPort int randomServerPort;

  @Test
  public void canCreateGame() throws URISyntaxException {
    final RestTemplate restTemplate = new RestTemplate();

    String baseUrl = "http://localhost:" + this.randomServerPort + "/v1/game/";
    final URI uri = new URI(baseUrl);
    final GameRequest gameRequest =
        new GameRequest(gameID); // GameRequest("1321","William", "Henry");

    final HttpHeaders headers = new HttpHeaders();
    headers.set("X-COM-PERSIST", "true");
    final ObjectMapper mapper = new ObjectMapper();
    final HttpEntity<GameRequest> request = new HttpEntity<>(gameRequest, headers);

    final ResponseEntity<String> result = restTemplate.postForEntity(uri, request, String.class);
    try {
      final String resultString = result.getBody();
      assert resultString != null;
      final Reader reader = new StringReader(resultString);

      final GameResponse gameResponse = mapper.readValue(reader, GameResponse.class);
      GameControllerTest.gameID = gameResponse.getGameId();

    } catch (final IOException e) {
      e.printStackTrace();
    }

    // Verify request succeed
    Assertions.assertEquals(200, result.getStatusCodeValue());
  }

  private ObjectMapper createObjectMapper() {

    final ObjectMapper objectMapper = new ObjectMapper();
    objectMapper.configure(MapperFeature.ACCEPT_CASE_INSENSITIVE_PROPERTIES, true);

    return objectMapper;
  }

  private MappingJackson2HttpMessageConverter createMappingJacksonHttpMessageConverter() {

    final MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
    converter.setObjectMapper(this.createObjectMapper());
    return converter;
  }

  // public RestTemplate createRestTemplate() {

  // RestTemplate restTemplate = new RestTemplate();
  // restTemplate.getMessageConverters().add(0, createMappingJacksonHttpMessageConverter());
  // }
  /*  @Test
       public void testAddGameMissingHeader() throws URISyntaxException
       {
           RestTemplate restTemplate = new RestTemplate();
           final String baseUrl = "http://localhost:"+randomServerPort+"/v1/game";
           URI uri = new URI(baseUrl);
           GameRequest gameRequest = new GameRequest("William", "Henry");
           HttpHeaders headers = new HttpHeaders();

           HttpEntity<GameRequest> request = new HttpEntity<>(gameRequest, headers);

           try
           {
               restTemplate.postForEntity(uri, request, String.class);
               Assertions.fail();
           }
           catch(HttpClientErrorException ex)
           {
               //Verify bad request and missing header
               Assertions.assertEquals(400, ex.getRawStatusCode());
               Assertions.assertEquals(true, ex.getResponseBodyAsString().contains("Missing request header"));
           }
       }

    /*   @Test
       public void testGetGameListSuccessWithHeaders() throws URISyntaxException
       {
           RestTemplate restTemplate = new RestTemplate();

           final String baseUrl = "http://localhost:"+randomServerPort+"/v1/game/";
           URI uri = new URI(baseUrl);

           HttpHeaders headers = new HttpHeaders();
           headers.set("X-COM-LOCATION", "USA");

           HttpEntity<GameRequest> requestEntity = new HttpEntity<>(null, headers);

           try
           {
               restTemplate.exchange(uri, HttpMethod.GET, requestEntity, String.class);
               Assertions.fail();
           }
           catch(HttpClientErrorException ex)
           {
               //Verify bad request and missing header
               Assertions.assertEquals(400, ex.getRawStatusCode());
               Assertions.assertEquals(true, ex.getResponseBodyAsString().contains("Missing request header"));
           }
       }
  */
  @ParameterizedTest(name = "gameId")
  @ValueSource(strings = {"", "  "})
  public void camAddMove(final String gameId, final Turn turnPlayer) throws URISyntaxException {
    final RestTemplate restTemplate = new RestTemplate();

    String baseUrl = "http://localhost:" + this.randomServerPort + "/games/";
    final URI uri = new URI(baseUrl);
    @NotNull final Turn Player = Turn.FIRST_PLAYER;
    final MoveRequest moveRequest =
        MoveRequest.builder().stateId(gameId).moveCellIndex(3).player(Player).build();

    final HttpHeaders headers = new HttpHeaders();
    headers.set("X-COM-PERSIST", "true");

    final HttpEntity<MoveRequest> request = new HttpEntity<>(moveRequest, headers);

    final ResponseEntity<String> result = restTemplate.postForEntity(uri, request, String.class);

    // Verify request succeed
    Assertions.assertEquals(201, result.getStatusCodeValue());
  }

  @ParameterizedTest(name = "gameId")
  @ValueSource(strings = {"", "  "})
  public void testAddInitialMove(final String gameId) throws URISyntaxException {
    final RestTemplate restTemplate = new RestTemplate();

    String baseUrl = "http://localhost:" + this.randomServerPort + "/games/";
    final URI uri = new URI(baseUrl);
    @NotNull final Turn Player = Turn.FIRST_PLAYER;
    final MoveRequest moveRequest =
        MoveRequest.builder().stateId(gameId).moveCellIndex(3).player(Player).build();

    final HttpHeaders headers = new HttpHeaders();
    headers.set("X-COM-PERSIST", "true");

    final HttpEntity<MoveRequest> request = new HttpEntity<>(moveRequest, headers);

    final ResponseEntity<String> result = restTemplate.postForEntity(uri, request, String.class);

    // Verify request succeed
    Assertions.assertEquals(201, result.getStatusCodeValue());
  }
}
