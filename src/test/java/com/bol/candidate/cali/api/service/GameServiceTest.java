package com.bol.candidate.cali.api.service;

import com.bol.candidate.cali.api.request.GameRequest;
import com.bol.candidate.cali.api.request.GameResponse;
import com.bol.candidate.cali.data.entity.Game;
import com.bol.candidate.cali.data.repo.GameRepository;
import lombok.NonNull;
import lombok.SneakyThrows;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.projection.SpelAwareProxyProjectionFactory;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
public class GameServiceTest {

  private static final String gameID = "demo";
  @NonNull @Mock private GameRepository gameRepository;

  @Mock private BoardStateService boardStateService;
  @Mock private GameService testingGameService;

  @Mock private GameRequest gameReq;
  @NonNull @Mock private SpelAwareProxyProjectionFactory projectionFactory;

  @SneakyThrows
  @BeforeEach

  void setUp() {

    this.testingGameService =
        new GameService(this.gameRepository, this.boardStateService, this.projectionFactory);
  }

  @Test
  void canCreateMultiPlayerGame() {
    // given

    gameReq = new GameRequest(gameID);
    gameReq.setGameId("FirstGameWithBol");

    // testingGameService.createSoloGame(gameReq);
    this.testingGameService.createMultiPlayerGame(this.gameReq);

    // expected
  }

  @Test
  void canCreateSoloGame() {
    {
      gameReq = new GameRequest(gameID);
      gameReq.setGameId("FirstGameWithBol");

      this.testingGameService.createSoloGame(this.gameReq);

      // assertThat(actual.getGameId().equals(this.gameReq.getGameId()));
    }
  }

  @Test
  void canInitSoloGame() {
    gameReq = new GameRequest(gameID);
    gameReq.setGameId("FirstGameWithBol");
    final Game actual = this.testingGameService.initSoloGame(this.gameReq);

    assertThat(actual.getGameId().equals(this.gameReq.getGameId()));
  }

  @Test
  void canFindGameByGameId() {
    gameReq = new GameRequest(gameID);

    gameReq.setGameId("FirstGameWithBol");
    this.testingGameService.createSoloGame(this.gameReq);

    GameResponse actual = this.testingGameService.findGameByGameId("FirstGameWithBol");

    assertThat(actual.getGameId().equals("FirstGameWithBol"));
  }

  @Test
  void canLoadActiveGameWithBoardStateId() {
    gameReq = new GameRequest(gameID);

    GameResponse actual =
        this.testingGameService.loadActiveGameWithBoardStateId("FirstGameWithBol");

    assertThat(actual.getGameId().equals("FirstGameWithBol"));
  }
}
