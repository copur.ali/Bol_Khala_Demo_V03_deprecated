package com.bol.candidate.cali;

import com.bol.candidate.cali.data.entity.BoardState;
import com.bol.candidate.cali.data.entity.Cell;
import com.bol.candidate.cali.data.enums.Turn;
import lombok.experimental.UtilityClass;

@UtilityClass
public class Utility {
  public static BoardState createState(final Turn turn) {
    if (turn.equals(Turn.FIRST_PLAYER)) return new BoardState();
    else {
      BoardState state = new BoardState();
      state.setTurn(Turn.SECOND_PLAYER);
      return state;
    }
  }

  public static Cell treasureCell() {
    return new Cell().withPieces(0);
  }

  public static BoardState win() {
    BoardState state = createState(Turn.FIRST_PLAYER);
    state.setWinnerFound(true);
    return state;
  }

  public static Cell isEmpty() {
    return new Cell().withPieces(0);
  }

  public static BoardState notZeroState(final Turn turn) {
    BoardState state = createState(turn);
    for (int i = 0; i < 5; i++) {
      state.getFirstPlayerRow().getCells().get(i).setPieces(0);
    }
    return state;
  }

  public static BoardState treasureCellState(Turn turn, int treasureCell) {
    BoardState state = createState(turn);

    if (turn.equals(Turn.SECOND_PLAYER)) {
      state.getSecondPlayerRow().getCells().get(treasureCell - 1).setPieces(1);
      state.getSecondPlayerRow().getCells().get(treasureCell).setPieces(0);
    } else {
      state.getFirstPlayerRow().getCells().get(treasureCell - 1).setPieces(1);
      state.getFirstPlayerRow().getCells().get(treasureCell).setPieces(0);
    }

    return state;
  }
}
