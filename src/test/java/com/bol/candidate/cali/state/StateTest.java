package com.bol.candidate.cali.state;

import com.bol.candidate.cali.Utility;
import com.bol.candidate.cali.api.request.MoveRequest;
import com.bol.candidate.cali.api.service.BoardStateService;
import com.bol.candidate.cali.api.service.CellService;
import com.bol.candidate.cali.data.entity.BoardState;
import com.bol.candidate.cali.data.enums.Turn;
import com.bol.candidate.cali.data.repo.BoardStateRepository;
import com.bol.candidate.cali.exception.BoardStateNotFoundException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.data.projection.SpelAwareProxyProjectionFactory;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import java.util.Optional;
import java.util.stream.IntStream;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doReturn;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
// @EnableAutoConfiguration(exclude = EmbeddedMongoAutoConfiguration.class)

@ActiveProfiles("test")
@DataMongoTest
// @ExtendWith(SpringExtension.class)
@DisplayName("State TEst Integration Test")
// @SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
// @ImportAutoConfiguration(exclude = EmbeddedMongoAutoConfiguration.class)
@EnableWebMvc
@ExtendWith(MockitoExtension.class)
public class StateTest {
  @InjectMocks private SpelAwareProxyProjectionFactory projectionFactory;

  protected MockMvc mockMvc;
  @Mock private BoardStateRepository stateRepository;

  @InjectMocks private BoardStateService stateService;

  @Mock private CellService cellService;

  @Test
  public void whenCreateNewBoardState_shouldReturnOk() throws Exception {
    mockMvc
        .perform(post("/boardstate"))
        .andExpect(status().isCreated())
        .andExpect(jsonPath("$.id").isNotEmpty())
        .andExpect(jsonPath("$.turn").value("FIRST_PLAYER"))
        .andExpect(jsonPath("$.firstPlayerRow").isNotEmpty())
        .andExpect(jsonPath("$.firstPlayerRow.cells[0].pieces").value(6))
        .andExpect(jsonPath("$.firstPlayerRow.cells[1].pieces").value(6))
        .andExpect(jsonPath("$.firstPlayerRow.cells[2].pieces").value(6))
        .andExpect(jsonPath("$.firstPlayerRow.cells[3].pieces").value(6))
        .andExpect(jsonPath("$.firstPlayerRow.cells[4].pieces").value(6))
        .andExpect(jsonPath("$.firstPlayerRow.cells[5].pieces").value(6))
        .andExpect(jsonPath("$.firstPlayerRow.treasureCell.pieces").value(0))
        .andExpect(jsonPath("$.secondPlayerRow").isNotEmpty())
        .andExpect(jsonPath("$.secondPlayerRow.cells[0].pieces").value(6))
        .andExpect(jsonPath("$.secondPlayerRow.cells[1].pieces").value(6))
        .andExpect(jsonPath("$.secondPlayerRow.cells[2].pieces").value(6))
        .andExpect(jsonPath("$.secondPlayerRow.cells[3].pieces").value(6))
        .andExpect(jsonPath("$.secondPlayerRow.cells[4].pieces").value(6))
        .andExpect(jsonPath("$.secondPlayerRow.cells[5].pieces").value(6))
        .andExpect(jsonPath("$.secondPlayerRow.treasureCell.pieces").value(0));
  }

  @Test
  public void whenGetBoardState_shouldReturnOk() throws Exception {
    BoardState state = stateRepository.save(new BoardState());

    mockMvc
        .perform(get("/boardstate/{id}", state.getId()))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.id").isNotEmpty())
        .andExpect(jsonPath("$.turn").value("FIRST_PLAYER"))
        .andExpect(jsonPath("$.firstPlayerRow").isNotEmpty())
        .andExpect(jsonPath("$.firstPlayerRow.cells[0].pieces").value(6))
        .andExpect(jsonPath("$.firstPlayerRow.cells[1].pieces").value(6))
        .andExpect(jsonPath("$.firstPlayerRow.cells[2].pieces").value(6))
        .andExpect(jsonPath("$.firstPlayerRow.cells[3].pieces").value(6))
        .andExpect(jsonPath("$.firstPlayerRow.cells[4].pieces").value(6))
        .andExpect(jsonPath("$.firstPlayerRow.cells[5].pieces").value(6))
        .andExpect(jsonPath("$.firstPlayerRow.treasureCell.pieces").value(0))
        .andExpect(jsonPath("$.secondPlayerRow").isNotEmpty())
        .andExpect(jsonPath("$.secondPlayerRow.cells[0].pieces").value(6))
        .andExpect(jsonPath("$.secondPlayerRow.cells[1].pieces").value(6))
        .andExpect(jsonPath("$.secondPlayerRow.cells[2].pieces").value(6))
        .andExpect(jsonPath("$.secondPlayerRow.cells[3].pieces").value(6))
        .andExpect(jsonPath("$.secondPlayerRow.cells[4].pieces").value(6))
        .andExpect(jsonPath("$.secondPlayerRow.cells[5].pieces").value(6))
        .andExpect(jsonPath("$.secondPlayerRow.treasureCell.pieces").value(0));
  }

  @Test
  public void whenFirstPlayerPlayAndDoNotEndInTreasuryCell_shouldNextBeSecondPlayerAndReturnOk()
      throws Exception {
    BoardState state = stateRepository.save(new BoardState());

    mockMvc
        .perform(
            put("/boardstate/{id}", state.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(
                    new ObjectMapper()
                        .writeValueAsString(
                            MoveRequest.builder()
                                .player(Turn.FIRST_PLAYER)
                                .moveCellIndex(4)
                                .build())))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.id").isNotEmpty())
        .andExpect(jsonPath("$.turn").value("SECOND_PLAYER"))
        .andExpect(jsonPath("$.firstPlayerRow").isNotEmpty())
        .andExpect(jsonPath("$.firstPlayerRow.cells[0].pieces").value(6))
        .andExpect(jsonPath("$.firstPlayerRow.cells[1].pieces").value(6))
        .andExpect(jsonPath("$.firstPlayerRow.cells[2].pieces").value(6))
        .andExpect(jsonPath("$.firstPlayerRow.cells[3].pieces").value(6))
        .andExpect(jsonPath("$.firstPlayerRow.cells[4].pieces").value(0))
        .andExpect(jsonPath("$.firstPlayerRow.cells[5].pieces").value(7))
        .andExpect(jsonPath("$.firstPlayerRow.treasureCell.pieces").value(1))
        .andExpect(jsonPath("$.secondPlayerRow").isNotEmpty())
        .andExpect(jsonPath("$.secondPlayerRow.cells[0].pieces").value(7))
        .andExpect(jsonPath("$.secondPlayerRow.cells[1].pieces").value(7))
        .andExpect(jsonPath("$.secondPlayerRow.cells[2].pieces").value(7))
        .andExpect(jsonPath("$.secondPlayerRow.cells[3].pieces").value(7))
        .andExpect(jsonPath("$.secondPlayerRow.cells[4].pieces").value(6))
        .andExpect(jsonPath("$.secondPlayerRow.cells[5].pieces").value(6))
        .andExpect(jsonPath("$.secondPlayerRow.treasureCell.pieces").value(0));
  }

  @Test
  public void whenSecondPlayerPlayAndDoNotEndInTreasuryCell_shouldNextBeFirstPlayerAndReturnOk()
      throws Exception {
    BoardState state = stateRepository.save(new BoardState());
    state.setTurn(Turn.SECOND_PLAYER);
    stateRepository.save(state);

    mockMvc
        .perform(
            put("/boardstate/{id}", state.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(
                    new ObjectMapper()
                        .writeValueAsString(
                            MoveRequest.builder()
                                .player(Turn.SECOND_PLAYER)
                                .moveCellIndex(4)
                                .build())))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.id").isNotEmpty())
        .andExpect(jsonPath("$.turn").value("FIRST_PLAYER"))
        .andExpect(jsonPath("$.firstPlayerRow").isNotEmpty())
        .andExpect(jsonPath("$.firstPlayerRow.cells[0].pieces").value(7))
        .andExpect(jsonPath("$.firstPlayerRow.cells[1].pieces").value(7))
        .andExpect(jsonPath("$.firstPlayerRow.cells[2].pieces").value(7))
        .andExpect(jsonPath("$.firstPlayerRow.cells[3].pieces").value(6))
        .andExpect(jsonPath("$.firstPlayerRow.cells[4].pieces").value(7))
        .andExpect(jsonPath("$.firstPlayerRow.cells[5].pieces").value(6))
        .andExpect(jsonPath("$.firstPlayerRow.treasureCell.pieces").value(0))
        .andExpect(jsonPath("$.secondPlayerRow").isNotEmpty())
        .andExpect(jsonPath("$.secondPlayerRow.cells[0].pieces").value(6))
        .andExpect(jsonPath("$.secondPlayerRow.cells[1].pieces").value(6))
        .andExpect(jsonPath("$.secondPlayerRow.cells[2].pieces").value(6))
        .andExpect(jsonPath("$.secondPlayerRow.cells[3].pieces").value(6))
        .andExpect(jsonPath("$.secondPlayerRow.cells[4].pieces").value(0))
        .andExpect(jsonPath("$.secondPlayerRow.cells[5].pieces").value(7))
        .andExpect(jsonPath("$.secondPlayerRow.treasureCell.pieces").value(1));
  }

  @Test
  public void whenEndsInEmptyCell_shouldCaptureMirroredStonesAndReturnOk() throws Exception {
    BoardState state = stateRepository.save(new BoardState());
    state.getFirstPlayerRow().getCells().get(3).setPieces(0);
    state.getFirstPlayerRow().getCells().get(2).setPieces(1);
    stateRepository.save(state);

    mockMvc
        .perform(
            put("/boardstate/{id}", state.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(
                    new ObjectMapper()
                        .writeValueAsString(
                            MoveRequest.builder()
                                .player(Turn.FIRST_PLAYER)
                                .moveCellIndex(2)
                                .build())))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.id").isNotEmpty())
        .andExpect(jsonPath("$.turn").value("SECOND_PLAYER"))
        .andExpect(jsonPath("$.firstPlayerRow.cells[0].pieces").value(6))
        .andExpect(jsonPath("$.firstPlayerRow.cells[1].pieces").value(6))
        .andExpect(jsonPath("$.firstPlayerRow.cells[2].pieces").value(0))
        .andExpect(jsonPath("$.firstPlayerRow.cells[3].pieces").value(7))
        .andExpect(jsonPath("$.firstPlayerRow.cells[4].pieces").value(6))
        .andExpect(jsonPath("$.firstPlayerRow.cells[5].pieces").value(6))
        .andExpect(jsonPath("$.firstPlayerRow.treasureCell.pieces").value(0))
        .andExpect(jsonPath("$.secondPlayerRow.cells[0].pieces").value(6))
        .andExpect(jsonPath("$.secondPlayerRow.cells[1].pieces").value(6))
        .andExpect(jsonPath("$.secondPlayerRow.cells[2].pieces").value(0))
        .andExpect(jsonPath("$.secondPlayerRow.cells[3].pieces").value(6))
        .andExpect(jsonPath("$.secondPlayerRow.cells[4].pieces").value(6))
        .andExpect(jsonPath("$.secondPlayerRow.cells[5].pieces").value(6))
        .andExpect(jsonPath("$.secondPlayerRow.treasureCell.pieces").value(0));
  }

  @Test
  public void whenCleanPlayerCells_shouldCompleteGameAndReturnOk() throws Exception {
    BoardState state = stateRepository.save(new BoardState());
    IntStream.range(0, 5).forEach(i -> state.getFirstPlayerRow().getCells().get(i).setPieces(0));
    stateRepository.save(state);

    mockMvc
        .perform(
            put("/boardstate/{id}", state.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(
                    new ObjectMapper()
                        .writeValueAsString(
                            MoveRequest.builder()
                                .player(Turn.FIRST_PLAYER)
                                .moveCellIndex(5)
                                .build())))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.id").isNotEmpty())
        .andExpect(jsonPath("$.completed").value("true"))
        .andExpect(jsonPath("$.firstPlayerRow.cells[0].pieces").value(0))
        .andExpect(jsonPath("$.firstPlayerRow.cells[1].pieces").value(0))
        .andExpect(jsonPath("$.firstPlayerRow.cells[2].pieces").value(0))
        .andExpect(jsonPath("$.firstPlayerRow.cells[3].pieces").value(0))
        .andExpect(jsonPath("$.firstPlayerRow.cells[4].pieces").value(0))
        .andExpect(jsonPath("$.firstPlayerRow.cells[5].pieces").value(0))
        .andExpect(jsonPath("$.firstPlayerRow.treasureCell.pieces").value(1))
        .andExpect(jsonPath("$.secondPlayerRow.cells[0].pieces").value(0))
        .andExpect(jsonPath("$.secondPlayerRow.cells[1].pieces").value(0))
        .andExpect(jsonPath("$.secondPlayerRow.cells[2].pieces").value(0))
        .andExpect(jsonPath("$.secondPlayerRow.cells[3].pieces").value(0))
        .andExpect(jsonPath("$.secondPlayerRow.cells[4].pieces").value(0))
        .andExpect(jsonPath("$.secondPlayerRow.cells[5].pieces").value(0))
        .andExpect(jsonPath("$.secondPlayerRow.treasureCell.pieces").value(41));
  }

  @Test
  public void whenEndsInTreasuryCell_shouldSamePlayerBeNextAndReturnOk() throws Exception {
    BoardState state = stateRepository.save(new BoardState());

    mockMvc
        .perform(
            put("/boardstate/{id}", state.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(
                    new ObjectMapper()
                        .writeValueAsString(
                            MoveRequest.builder()
                                .player(Turn.FIRST_PLAYER)
                                .moveCellIndex(0)
                                .build())))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.id").isNotEmpty())
        .andExpect(jsonPath("$.turn").value("FIRST_PLAYER"))
        .andExpect(jsonPath("$.firstPlayerRow").isNotEmpty())
        .andExpect(jsonPath("$.firstPlayerRow.cells[0].pieces").value(0))
        .andExpect(jsonPath("$.firstPlayerRow.cells[1].pieces").value(7))
        .andExpect(jsonPath("$.firstPlayerRow.cells[2].pieces").value(7))
        .andExpect(jsonPath("$.firstPlayerRow.cells[3].pieces").value(7))
        .andExpect(jsonPath("$.firstPlayerRow.cells[4].pieces").value(7))
        .andExpect(jsonPath("$.firstPlayerRow.cells[5].pieces").value(7))
        .andExpect(jsonPath("$.firstPlayerRow.treasureCell.pieces").value(1))
        .andExpect(jsonPath("$.secondPlayerRow").isNotEmpty())
        .andExpect(jsonPath("$.secondPlayerRow.cells[0].pieces").value(6))
        .andExpect(jsonPath("$.secondPlayerRow.cells[1].pieces").value(6))
        .andExpect(jsonPath("$.secondPlayerRow.cells[2].pieces").value(6))
        .andExpect(jsonPath("$.secondPlayerRow.cells[3].pieces").value(6))
        .andExpect(jsonPath("$.secondPlayerRow.cells[4].pieces").value(6))
        .andExpect(jsonPath("$.secondPlayerRow.cells[5].pieces").value(6))
        .andExpect(jsonPath("$.secondPlayerRow.treasureCell.pieces").value(0));
  }

  @Test
  public void whenOK_shouldReturnDefaultBoard() {
    doReturn(Optional.of(Utility.createState(Turn.FIRST_PLAYER)))
        .when(stateRepository)
        .findById(anyString());

    BoardState state = stateService.findById("1L");

    assertThat(state, StateHelper.firstNextMove());
  }

  @Test
  public void whenNotFound_shouldReturnDefaultBoard() {
    doReturn(Optional.empty()).when(stateRepository).findById(anyString());

    assertThrows(BoardStateNotFoundException.class, () -> stateService.findById("1L"));
  }

  @Test
  public void whenMatchNotFound_shouldReturnNotFound() throws Exception {
    mockMvc
        .perform(
            put("/boardstate/{id}", "99")
                .contentType(MediaType.APPLICATION_JSON)
                .content(
                    new ObjectMapper()
                        .writeValueAsString(
                            MoveRequest.builder()
                                .player(Turn.FIRST_PLAYER)
                                .moveCellIndex(4)
                                .build())))
        .andExpect(status().isNotFound());
  }

  @Test
  public void whenWrongPlay_shouldReturnBadRequest() throws Exception {
    BoardState state = stateRepository.save(new BoardState());

    mockMvc
        .perform(
            put("/boardstate/{id}", state.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(
                    new ObjectMapper()
                        .writeValueAsString(
                            MoveRequest.builder()
                                .player(Turn.SECOND_PLAYER)
                                .moveCellIndex(4)
                                .build())))
        .andExpect(status().isBadRequest());
  }

  @Test
  public void whenNegativeIndex_shouldReturnBadRequest() throws Exception {
    BoardState state = stateRepository.save(new BoardState());

    mockMvc
        .perform(
            put("/boardstate/{id}", state.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(
                    new ObjectMapper()
                        .writeValueAsString(
                            MoveRequest.builder()
                                .player(Turn.FIRST_PLAYER)
                                .moveCellIndex(-1)
                                .build())))
        .andExpect(status().isBadRequest());
  }

  @Test
  public void whenEmptyPlayer_shouldReturnBadRequest() throws Exception {
    BoardState state = stateRepository.save(new BoardState());

    mockMvc
        .perform(
            put("/boardstate/{id}", state.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(
                    new ObjectMapper()
                        .writeValueAsString(MoveRequest.builder().moveCellIndex(0).build())))
        .andExpect(status().isBadRequest());
  }
}
