package com.bol.candidate.cali.state;

import com.bol.candidate.cali.data.entity.BoardState;
import com.bol.candidate.cali.data.enums.Turn;
import org.hamcrest.Matcher;

import static org.hamcrest.Matchers.*;

public class StateHelper {

  public static Matcher<BoardState> turnNext() {
    return hasProperty("next", equalTo(Turn.SECOND_PLAYER));
  }

  public static Matcher<BoardState> firstNextMove() {
    return hasProperty("next", equalTo(Turn.FIRST_PLAYER));
  }

  public static Matcher<BoardState> capturePieces(Turn turn) {
    return hasProperty(
        Turn.FIRST_PLAYER.equals(turn) ? "FIRST_PLAYER" : "SECOND_PLAYER",
        hasProperty("cells", hasItem(hasProperty("pieces", equalTo(7)))));
  }

  public static Matcher<BoardState> gameCompleted() {
    return hasProperty("completed", equalTo(true));
  }
}
